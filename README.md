# openpyxl #

The repository has moved to https://foss.heptapod.net/openpyxl/openpyxl because Atlassian decided it wasn't
worth it to keep supporting the Mercurial version control system.
